<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Datenbetrieb.' . $_EXTKEY,
	'List',
	array(
		'Provider' => 'list',
	)
);
